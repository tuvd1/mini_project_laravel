@extends('common.app')
@section('contents')
    <div id="kt_content_container" class="container-xxl">
        <form action="{{ route('positions.update', $position->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div>
                <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Name</label>
                <input type="text" name="name" value="{{ $position->name }}"  class="form-control w-75"/>
            </div>
            <button type="submit" class="mt-3 btn btn-primary">Update</button>
        </form>
    </div>
@endsection
