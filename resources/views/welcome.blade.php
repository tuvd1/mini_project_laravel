<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <title>Document</title>
    <style>
        body{
            background-image: url("{{ asset('iron.jpeg') }}");
            background-attachment: fixed;
            background-size: auto 100%;
            background-position: center;
        }
        .login{
            /*color: black;*/
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
        }
        .min-width {
            min-width: 170px;
        }
        .color-btn {
            background-color: #56c497 !important;
            border: none;
        }
    </style>
</head>
<body>
@if (Route::has('login'))
    <div class="login">
        @auth
            <a type="button" class="btn btn-primary" href="{{ url('/home') }}" class="min-width">Home</a>
        @else
            <a type="button" href="{{ route('login') }}" class=" min-width btn btn-primary color-btn">Log in</a>
            @if (Route::has('register'))
                <a href="{{ route('register') }}" style="color: #fff"  class="min-width btn btn-info">Register</a>
            @endif
        @endauth
    </div>
@endif
</body>
</html>
<script>
    import About from "../../public/dist/pages/company/about.html";
    export default {
        components: {About}
    }
</script>
