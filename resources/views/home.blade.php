@extends('common.app')
@section('contents')
 <!--begin::Content-->
 <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Row-->
            <!--end::Row-->
            <!--begin::Tables Widget 9-->
            <div class="card mb-5 mb-xl-8">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder fs-3 mb-1">Employees List</span>
                    </h3>
                </div>
                <!--end::Header-->
                @if (session('message'))
                <div class="alert alert-success">{{ session('message') }}</div>
                @endif
                <!--begin::Body-->
                <div class="card-body py-3">
                    <!--begin::Table container-->
                    <div class="table-responsive">
                        <!--begin::Table-->
                        <!--begin::Table-->
                        <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                            <!--begin::Table head-->
                            <thead>
                                <tr class="fw-bolder text-muted">
                                    <th class="w-25px">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                <input class="form-check-input widget-9-check" type="checkbox" value="1">
                                            </div>
                                    </th>
                                    <th class="min-w-120px">FullName</th>
                                    <th class="min-w-120px">Email</th>
                                    <th class="min-w-120px">Phone</th>
                                    <th class="min-w-120px">Salary</th>
                                    <th class="min-w-120px">Department</th>
                                    <th class="min-w-120px">Position</th>
                                    <th class="min-w-120px">User</th>
                                </tr>
                            </thead>
                            <!--end::Table head-->
                            <!--begin::Table body-->
                                <tbody>
                                    @foreach ($employees as $employee)
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                <input class="form-check-input widget-9-check" type="checkbox" value="1">
                                            </div>
                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder fs-6">{{ $employee->name }}</a>

                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder fs-6">{{ $employee->email }}</a>

                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder fs-6">{{ $employee->phone }}</a>

                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder fs-6">{{ $employee->salary }}</a>

                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder fs-6">{{ $employee->department->name }}</a>
                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder fs-6">{{ $employee->position->name }}</a>
                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder fs-6">{{ $employee->user->name }}</a>
                                        </td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            
                            <!--end::Table body-->
                        </table>
                        <!--end::Table-->
                        <!--end::Table-->
                    </div>
                    <!--end::Table container-->
                </div>
                <!--begin::Body-->
            </div>
            <!--end::Tables Widget 9-->

        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
</div>
<!--end::Content-->
@endsection