<div style="padding-top: 20px" id="kt_content_container" class="container-xxl">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif                          
    <form action="{{ route('departments.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @include('components.input', ['name' => 'code', 'nameLabel' =>'Department Code' ])
        @include('components.input', ['name' => 'name', 'nameLabel' =>'Department Name' ])
        <button type="submit" class="mt-3 btn btn-primary">Save</button>
    </form>

</div>