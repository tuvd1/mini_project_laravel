<div style="padding-top: 20px" id="kt_content_container" class="container-xxl">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{ route('employees.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @include('components.input', ['name' => 'name', 'nameLabel' =>'Name' ])
        @include('components.input', ['name' => 'email', 'nameLabel' =>'Email' ])
        @include('components.input', ['name' => 'phone', 'nameLabel' =>'Phone' ])
        @include('components.input', ['name' => 'salary', 'nameLabel' =>'Salary' ])
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Select Department</label>
            <select name="department_id" class="form-control w-75">
                @foreach ($departments as $department)
                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Select User</label>
            <select name="user_id" class="form-control w-75">
                @foreach ($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Select Position</label>
            <select name="position_id" class="form-control w-75">
                @foreach ($positions as $position)
                    <option value="{{ $position->id }}">{{ $position->name }}</option>
                @endforeach
            </select>
        </div>
        {{-- @include('components.input', ['name' => 'position', 'nameLabel' =>'Position' ]) --}}
        <button type="submit" class="mt-3 btn btn-primary">Save</button>
    </form>
</div>
