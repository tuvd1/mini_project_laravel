<div id="kt_content_container" class="container-xxl">
    <form action="{{ route('user.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @include('components.input', ['name' => 'name', 'nameLabel' =>'User name' ])
        @include('components.input', ['name' => 'email', 'nameLabel' =>'Email' ])
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Select Department</label>
            <select name="role" class="form-control w-75">
                <option value="1">Admin</option>
                <option value="0">User</option>
            </select>
        </div>
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">PassWord</label>
            <input type="password" name="password"  class="form-control w-75"/>
        </div>
        <button type="submit" class="mt-3 btn btn-primary">Save</button>
    </form>
</div>
