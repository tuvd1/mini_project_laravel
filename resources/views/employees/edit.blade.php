@extends('common.app')
@section('contents')
<div id="kt_content_container" class="container-xxl">
    <form action="{{ route('employees.update', $employee->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Name</label>
            <input type="text" name="name" value="{{ $employee->name }}"  class="form-control w-75"/>
        </div>
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Email</label>
            <input type="text" name="email" value="{{ $employee->email }}"  class="form-control w-75"/>
        </div>
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Phone</label>
            <input type="text" name="phone" value="{{ $employee->phone }}"  class="form-control w-75"/>
        </div>
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Salary</label>
            <input type="text" name="salary" value="{{ $employee->salary }}"  class="form-control w-75"/>
        </div>
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Department</label>
            <select name="department_id" class="form-control w-75">
                @foreach ($departments as $department)
                    <option value="{{ $department->id }}"  {{ $employee->department_id === $department->id ? 'selected' : '' }}>
                        {{ $department->name }}
                    </option>
                @endforeach
            </select>
        </div>
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">User</label>
            <select name="user_id" class="form-control w-75">
                @foreach ($users as $user)
                    <option value="{{ $user->id }}" {{ $employee->user_id === $user->id ? 'selected' : '' }}>
                        {{ $user->name }}
                    </option>
                @endforeach
            </select>
        </div>
        <div>
            <label class="d-flex flex-column text-dark fw-bolder fs-3 mb-0 fs-5 fw-bold" style="padding-left: 7px;" for="">Position</label>
            <select name="position_id" class="form-control w-75">
                @foreach ($positions as $position)
                    <option value="{{ $position->id }}"  {{ $employee->position_id === $position->id ? 'selected' : '' }}>
                        {{ $position->name }}
                    </option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="mt-3 btn btn-primary">Update</button>
    </form>
</div>
@endsection
