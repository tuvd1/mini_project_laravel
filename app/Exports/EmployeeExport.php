<?php

namespace App\Exports;

use App\Models\Employee;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class EmployeeExport implements FromCollection, WithHeadings, WithMapping
{
    public function collection()
    {
        return Employee::with(['department','position','user'])->get();
    }

    public function map($employee) : array {
        return [
            $employee->id,
            $employee->name,
            $employee->email,
            $employee->phone,
            $employee->salary,
            $employee->department->name,
            $employee->position->name,
            $employee->user->name,
        ] ;
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function headings(): array
    {
        return ["ID", "Name", "Email", "Phone", "Salary", "Department", "Position", "User"];
    }
}
