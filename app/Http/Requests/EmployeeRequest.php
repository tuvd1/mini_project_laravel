<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6',
            'email' => 'required|email',
            'salary' => 'integer'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên nhân viên không được để trống',
            'name.min' => 'Tên nhân viên không được ít hơn :min kí tự',
            'email.required' => 'Email không được để trống',
            'email.email' => 'Bạn phải nhập đúng email',
            'salary.integer' => 'Bạn phải nhập đúng định dạng Salary'
        ];
    }
}
