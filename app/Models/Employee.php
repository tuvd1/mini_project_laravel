<?php

namespace App\Models;
use App\Models\User;
use App\Models\Department;
use App\Models\Position;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $table = 'employees';

    protected $fillable = [
        'name',
        'phone',
        'email',
        'salary',
        'user_id',
        'department_id',
        'position_id'
    ];
    
    public function department()
    {
        return $this->belongsTo(Department::class,'department_id','id');
    }
    public function position()
    {
        return $this->belongsTo(Position::class,'position_id','id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
