<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $position = [
            [
                'name' => 'Giam Doc'
            ],
            [
                'name' => 'Nhan Vien'
            ],
            [
                'name' => 'Bao Ve'
            ],
        ];
        foreach($position as $key => $value) {
            Position::create($value);
        }
    }
}
