<?php

namespace Database\Seeders;
use App\Models\Employee;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee = [
            [
                'name'  => 'Nguyen Ba Tien',
                'email' => 'tientienti@gmail.com',
                'phone' => '+8467924344',
                'salary'=> '10.000.000',
                'user_id' => 1,
                'department_id' => 1,
                'position_id' => 1
            ],
            [
                'name'  => 'Nguyen Ba Hoang',
                'email' => 'hoangtienti@gmail.com',
                'phone' => '+8443224344',
                'salary'=> '7.000.000',
                'user_id' => 2,
                'department_id' => 2,
                'position_id' => 2
            ],
            [
                'name'  => 'Nguyen Le Hieu',
                'email' => 'hieu@gmail.com',
                'phone' => '+84432332344',
                'salary'=> '8.000.000',
                'user_id' => 3,
                'department_id' => 3,
                'position_id' => 3
            ],


        ];
        foreach($employee as $key => $value) {
            Employee::create($value);
        }
    }
}
