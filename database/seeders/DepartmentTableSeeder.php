<?php

namespace Database\Seeders;
use App\Models\Department;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department = [
            [
                'code' => 'DP112',
                'name' => 'CNTT'
            ],
            [
                'code' => 'DP115',
                'name' => 'Kinh Doanh'
            ],
            [
                'code' => 'DP117',
                'name' => 'Quan Ly'
            ],
        ];
        foreach($department as $key => $value) {
            Department::create($value);
        }
    }
   
}
