<?php
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('employees', EmployeeController::class)->middleware(['auth','isAdmin']);
Route::resource('departments', DepartmentController::class)->middleware(['auth','isAdmin']);
Route::resource('positions', \App\Http\Controllers\PositionController::class)->middleware(['auth','isAdmin']);

Auth::routes();

Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('export',[EmployeeController::class,'exportEmployee'])->name('export');

Route::get('users-export', [UserController::class, 'export'])->name('user.export');

Route::get('users', [UserController::class,'index'])->name('user.index')->middleware(['auth','isAdmin']);
Route::get('users/create', [UserController::class,'create'])->name('user.create')->middleware(['auth','isAdmin']);
Route::post('users/store', [UserController::class,'store'])->name('user.store')->middleware(['auth','isAdmin']);
Route::delete('users/{user}', [UserController::class,'destroy'])->name('user.destroy')->middleware(['auth','isAdmin']);
